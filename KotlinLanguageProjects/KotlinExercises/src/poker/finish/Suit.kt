package poker.finish

enum class Suit {
    S, C, H, D
}