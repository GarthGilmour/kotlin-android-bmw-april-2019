package videostore.finish

enum class PriceCode {
    CHILDRENS,
    REGULAR,
    NEW_RELEASE
}