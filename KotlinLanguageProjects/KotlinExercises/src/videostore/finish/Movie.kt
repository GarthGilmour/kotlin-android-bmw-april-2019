package videostore.finish

import videostore.finish.PriceCode.CHILDRENS
import videostore.finish.PriceCode.NEW_RELEASE
import videostore.finish.PriceCode.REGULAR

class Movie(val title: String, val priceCode: PriceCode) {
    fun cost(daysRented: Int): Double {
        var cost = 0.0
        when (priceCode) {
            REGULAR -> {
                cost += 2
                if (daysRented > 2) {
                    cost += (daysRented - 2) * 1.5
                }
            }
            NEW_RELEASE -> cost += daysRented * 3
            CHILDRENS -> {
                cost += 1.5
                if (daysRented > 3) {
                    cost += (daysRented - 3) * 1.5
                }
            }
        }
        return cost
    }
    fun points(daysRented: Int): Int {
        // add bonus for a two day new release rental
        if ((priceCode == NEW_RELEASE) && daysRented > 1) {
            return 2
        }
        return 1
    }
}