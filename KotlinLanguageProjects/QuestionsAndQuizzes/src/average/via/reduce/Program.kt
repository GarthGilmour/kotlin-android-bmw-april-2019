package average.via.reduce

fun main() {
    val numbers = listOf(10, 20, 30, 40, 50, 60, 70, 80, 90, 100)
    println(averageV1(numbers))
    println(averageV2(numbers))
    println(averageV3(numbers))
}

fun averageV1(input: List<Int>): Double {
    val total = input.reduce { a, b -> a + b }
    val sum = input.fold(0.0) { a, _ -> a + 1 }
    return total / sum
}

fun averageV2(input: List<Int>): Double {
    val results = input.fold(Pair(0, 0.0)) { state, number ->
        val (count, sum) = state
        Pair(count + 1, sum + number)
    }
    return results.second / results.first
}

fun averageV3(input: List<Int>): Double {
    val (totalCount, totalSum) = input.fold(Pair(0, 0.0)) { (count, sum), number ->
        Pair(count + 1, sum + number)
    }
    return totalSum / totalCount
}