package labels

fun main() {
    showLabelsOnLoops()
    showLabelsOnLambdas()
}

fun showLabelsOnLoops() {
    println("----- Labels on Loops -----")

    var foo = 0
    val bar = 100

    fred@ while(true) {
        println("In outer loop")
        while(true) {
            println(++foo)
            if(foo % 10 == 0) {
                break
            }
            if(foo > bar) {
                break@fred
            }
        }
    }
    println("Bye...")
}

fun showLabelsOnLambdas() {
    println("----- Labels on Lambdas -----")

    (1..20).forEach fred@{
        println("Start of $it")
        if(it > 10) return@fred
        println("End of $it")
    }
    ('a'..'z').forEach {
        println("Start of $it")
        if(it > 'h') return@forEach
        println("End of $it")
    }
}