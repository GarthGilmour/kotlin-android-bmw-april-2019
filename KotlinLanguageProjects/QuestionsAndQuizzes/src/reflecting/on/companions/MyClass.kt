package reflecting.on.companions

class MyClass {
    fun foo() = "foo"
    fun bar() = "bar"
    companion object MyCompanion {
        fun wibble() = "wibble"
        fun wobble() = "wobble"
    }
}