package reflecting.on.companions

import kotlin.reflect.KClass
import kotlin.reflect.full.companionObject
import kotlin.reflect.full.declaredMemberFunctions

fun main() {
    reflectUsingKoltinAPI(MyClass::class)
    reflectUsingJavaAPI(MyClass::class.java)
    reflectUsingJavaAPI(MyClass.MyCompanion::class.java)
}

fun <T : Any> reflectUsingKoltinAPI(kClass: KClass<T>) {
    println("---------- Kotlin Reflection ----------")
    println("This is '${kClass.qualifiedName}'")

    println("With methods:")
    kClass.declaredMemberFunctions
        .forEach { println("\t${it.name}") }

    val companionKClass = kClass.companionObject
    if (companionKClass != null) {
        println("The companion object is: '${companionKClass.qualifiedName}'")
        println("With methods:")
        companionKClass.declaredMemberFunctions
            .forEach { println("\t${it.name}") }
    }
}

fun <T> reflectUsingJavaAPI(klass: Class<T>) {
    println("---------- Java Reflection ----------")
    println("This is '${klass.name}'")
    println("With methods:")
    klass.declaredMethods
        .forEach { println("\t${it.name}") }
}
