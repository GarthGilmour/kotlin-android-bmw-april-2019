package comparing.enums

fun main() {
    printTypeDetails(ChessPiecesKotlin::class.java)
    printTypeDetails(ChessPiecesJava::class.java)
}

fun <T> printTypeDetails(klass: Class<T>) {
    println("This is ${klass.name}")
    println("Inheriting from ${klass.superclass.name}")
    println("Fields are:")
    klass.declaredFields.forEach { field ->
        println("\t${field.name}")
    }
    println("Methods are:")
    klass.declaredMethods.forEach { method ->
        println("\t${method.name}")
    }
    println("------------------")
}