package comparing.enums

enum class ChessPiecesKotlin {
    PAWN,
    ROOK,
    KNIGHT,
    BISHOP,
    QUEEN,
    KING
}