package comparing.enums;

public enum ChessPiecesJava {
    PAWN,
    ROOK,
    KNIGHT,
    BISHOP,
    QUEEN,
    KING
}
