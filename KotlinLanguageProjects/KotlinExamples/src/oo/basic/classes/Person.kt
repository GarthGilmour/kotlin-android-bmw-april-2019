package oo.basic.classes

class Person(public val name: String)

fun main() {
    val p1 = Person("Jane")
    val p2 = Person("Dave")

    println(p1.name)
    println(p2.name)
}