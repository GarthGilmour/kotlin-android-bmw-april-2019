package coroutines.async.v3

import coroutines.shared.pingSlowServerAsync
import kotlinx.coroutines.*
import java.time.LocalTime
import java.time.temporal.ChronoUnit.SECONDS

fun main() = runBlocking {
    println("Program starts")
    runDemo().join()
    println("Program ends")
}

fun runDemo() = GlobalScope.launch {
    println("Work starts")
    val start = LocalTime.now()

    val deferredJobs = mutableListOf<Deferred<String>>()
    deferredJobs += async { pingSlowServerAsync(8) }
    deferredJobs += async { pingSlowServerAsync(6) }
    deferredJobs += async { pingSlowServerAsync(4) }
    deferredJobs += async { pingSlowServerAsync(2) }

    val results = deferredJobs
        .map { it.await() }
        .joinToString(
            prefix = "\"",
            postfix = "\"",
            separator = ", " )
    println("Results are $results")

    val stop = LocalTime.now()
    println("Work complete in ${start.until(stop,SECONDS)} seconds")
}



