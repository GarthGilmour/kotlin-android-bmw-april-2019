package coroutines.shared

import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

fun pingSlowServer(timeout: Int): String {
    val client = ClientBuilder.newClient()
    val result = client
            .target("http://localhost:8080")
            .path("ping")
            .path(timeout.toString())
            .request(MediaType.APPLICATION_JSON)
            .get(Array<String>::class.java)

    return result?.get(0) ?: "No Response"
}

suspend fun pingSlowServerAsync(timeout: Int): String {
    return suspendCoroutine { continuation ->
        val client = ClientBuilder.newClient()
        client
            .target("http://localhost:8080")
            .path("ping")
            .path(timeout.toString())
            .request(MediaType.APPLICATION_JSON)
            .async()
            .get(object : InvocationCallback<Array<String>> {
                override fun completed(response: Array<String>?) {
                    continuation.resume(response?.get(0) ?: "No Response")
                    client.close()
                }
                override fun failed(throwable: Throwable?) {
                    continuation.resumeWithException(throwable as Exception)
                    client.close()
                }
            })
    }
}