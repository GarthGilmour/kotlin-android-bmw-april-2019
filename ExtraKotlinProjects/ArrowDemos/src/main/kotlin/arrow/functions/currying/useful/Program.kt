package arrow.functions.currying.useful

import arrow.syntax.function.curried
import java.io.BufferedReader
import java.io.FileReader

fun grep(path: String, regex: Regex, action: (String) -> Unit) {
    val reader = BufferedReader(FileReader(path))
    reader.use {
        it.lines()
            .filter { regex.matches(it) }
            .forEach(action)
    }
}

val grepLambda = { a: String, b: Regex, c: (String) -> Unit -> grep(a,b,c) }

fun printLine() = println("-------------")

fun main(args: Array<String>) {
    val filePath = "data/grepInput.txt"
    val regex1 = "[A-Z]{2}[0-9]{2}".toRegex()
    val regex2 = "[a-z]{2}[0-9]{2}".toRegex()

    val f1 = grepLambda.curried()
    val grepInFile = f1(filePath)
    val grepRegex1 = grepInFile(regex1)
    val grepRegex2 = grepInFile(regex2)

    grepRegex1(::println)
    printLine()
    grepRegex2(::println)
}