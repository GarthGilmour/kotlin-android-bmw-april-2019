package arrow.functions.partial.application.useful

import arrow.syntax.function.partially2
import arrow.syntax.function.partially3
import java.io.BufferedReader
import java.io.FileReader

fun grep(path: String, regex: Regex, action: (String) -> Unit) {
    val reader = BufferedReader(FileReader(path))
    reader.use {
        it.lines()
            .filter { regex.matches(it) }
            .forEach(action)
    }
}

val grepLambda = { a: String, b: Regex, c: (String) -> Unit ->
    grep(a, b, c)
}

fun printLine() = println("-------------")

fun main(args: Array<String>) {
    val filePath = "data/grepInput.txt"
    val regex = "[A-Z]{2}[0-9]{2}".toRegex()

    grep(filePath, regex, ::println)
    printLine()

    grepLambda(filePath, regex, ::println)
    printLine()

    val grepAndPrint = grepLambda.partially3(::println)
    grepAndPrint(filePath, regex)
    printLine()

    val sb = StringBuilder()
    val grepAndConcat = grepLambda.partially3 {sb.append(it)}
    grepAndConcat(filePath, regex)
    println(sb.toString())
    printLine()

    val grepAndPrintRegex = grepAndPrint.partially2(regex)
    grepAndPrintRegex(filePath)
    printLine()
}