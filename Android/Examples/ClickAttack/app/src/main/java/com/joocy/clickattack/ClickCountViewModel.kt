package com.joocy.clickattack

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class ClickCountViewModel : ViewModel() {

    val clickCount: MutableLiveData<Int> = MutableLiveData<Int>().apply { value = 0 }

    fun incrementCount() {
        val newCount = clickCount.value?.plus(1) ?: 1
        clickCount.value = newCount
    }

    fun reset() {
        clickCount.value = 0
    }
}