package com.joocy.clickattack

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.joocy.clickattack.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel: ClickCountViewModel = ViewModelProviders.of(this).get(ClickCountViewModel::class.java)

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        val clickCountObserver = Observer<Int> { newClickCount ->
            clickCountText.text = newClickCount.toString()
        }

        viewModel.clickCount.observe(this, clickCountObserver)
    }

}
