package com.joocy.clickattack

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.filters.LargeTest
import android.support.test.rule.ActivityTestRule
import android.widget.TextView
import org.hamcrest.CoreMatchers.allOf
import org.junit.Rule
import org.junit.Test

@LargeTest
class MainActivityTest {

    @Rule
    @JvmField
    var rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun clickingOnSplatIncrementsCount() {
        val previousClickCount = currentlyDisplayedCount()
        clickSplat()
        checkThatTotalIsDisplayed(previousClickCount + 1)
    }

    @Test
    fun clickOnResetDisplaysZero() {
        clickSplat()
        clickSplat()
        onView(withId(R.id.resetButton)).perform(click())
        checkThatTotalIsDisplayed(0)
    }

    private fun clickSplat() {
        onView(withId(R.id.splat)).perform(click())
    }

    private fun checkThatTotalIsDisplayed(total: Int) {
        onView(withId(R.id.clickCountText)).check(matches(withText(total.toString())))
    }

    private fun currentlyDisplayedCount(): Int {
        val count = rule.activity.findViewById<TextView>(R.id.clickCountText).text.toString()
        return count.toInt()
    }

}