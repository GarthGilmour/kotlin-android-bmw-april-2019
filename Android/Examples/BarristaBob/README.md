## Overview
Barista-Bob a coffee ordering app currently under development for android, which demonstrates
a number of best practices along with pulling in some of the most commonly used Android Jetpack
libraries. Designed for Possible reuse at KotlinConf.


## Technology Stack

* Android API 28
* Kotlin
* Dagger (https://github.com/google/dagger)
* DataBinding Library (https://developer.android.com/topic/libraries/data-binding/index.html)
* Detekt (https://github.com/arturbosch/detekt)
* Ktlint (https://github.com/shyiko/ktlint)
* Mockito (https://github.com/mockito/mockito)
* Espresso
* MultiDex
* Retrofit
* RxJava
* okhttp

## Building

To build the application, execute the following:
```
$ ./gradlew clean build
```