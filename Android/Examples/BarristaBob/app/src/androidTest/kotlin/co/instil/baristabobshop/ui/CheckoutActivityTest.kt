/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.ui

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import co.instil.baristabobshop.R
import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.checkout.activity.CheckoutActivity
import co.instil.baristabobshop.checkout.service.CheckoutService
import co.instil.baristabobshop.dagger.DaggerApplicationInitializer
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.espresso.assertThatActionBarTitleIsSetTo
import co.instil.baristabobshop.espresso.assertThatRadioButtonIsNotChecked
import co.instil.baristabobshop.espresso.assertThatRecyclerViewHasChildCount
import co.instil.baristabobshop.espresso.assertThatViewIsDisplayed
import co.instil.baristabobshop.espresso.assertThatViewIsEnabled
import co.instil.baristabobshop.espresso.assertThatViewIsNotEnabled
import co.instil.baristabobshop.espresso.clickView
import co.instil.baristabobshop.navigation.NavigationRouter
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class CheckoutActivityTest {

    @Rule @JvmField val activityTestRule = ActivityTestRule<CheckoutActivity>(CheckoutActivity::class.java, true, false)
    @Rule @JvmField val daggerApplicationRule = DaggerApplicationInitializer(this)

    @Inject lateinit var resourceService: ResourceService
    @Inject lateinit var checkoutService: CheckoutService
    @Inject lateinit var navigationRouter: NavigationRouter

    private val drinkOrderList = listOf(
        DrinkOrderDetails(2, 5, createDrink()),
        DrinkOrderDetails(6, 5, createDrink())
    )

    @Before
    fun beforeEachTest() {
        given(resourceService.getStringResource(R.string.format_currency_gbp)).willReturn("£%.2f")
        given(resourceService.getStringResource(R.string.format_quantity)).willReturn("x %s")
        given(checkoutService.getAllDrinkOrderDetails()).willReturn(Single.just(drinkOrderList))
    }

    @Test
    fun shouldDisplayActionBar() {
        launchActivity()

        assertThatViewIsDisplayed(R.id.action_bar)
    }

    @Test
    fun shouldDisplayTotalOrderCostText() {
        launchActivity()

        assertThatViewIsDisplayed(R.id.orderTotalTitleText)
    }

    @Test
    fun shouldDisplayTotalOrderCostNumber() {
        launchActivity()

        assertThatViewIsDisplayed(R.id.orderTotalNumberText)
    }

    @Test
    fun shouldDisplayBaristaBobLogo() {
        launchActivity()

        assertThatViewIsDisplayed(R.id.baristaBobLogo)
    }

    @Test
    fun actionBarTitleShouldBeSet() {
        launchActivity()

        assertThatActionBarTitleIsSetTo(activityTestRule.activity.supportActionBar!!, "Checkout")
    }

    @Test
    fun recyclerViewShouldHaveTwoChildrenFromTheServiceAfterLaunch() {
        launchActivity()

        assertThatRecyclerViewHasChildCount(R.id.checkoutRecyclerView, 2)
    }

    @Test
    fun shouldHaveUncheckedRadioButtons() {
        launchActivity()

        assertThatRadioButtonIsNotChecked(R.id.payByCashRadioButton)
        assertThatRadioButtonIsNotChecked(R.id.payByCardRadioButton)
    }

    @Test
    fun shouldHaveDisabledConfirmButtonBeforeSelectingAPaymentMethod() {
        launchActivity()

        assertThatViewIsNotEnabled(R.id.confirmPurchaseButton)
    }

    @Test
    fun shouldHaveEnabledConfirmButtonAfterSelectingPayByCash() {
        launchActivity()

        clickView(R.id.payByCashRadioButton)

        assertThatViewIsEnabled(R.id.confirmPurchaseButton)
    }

    @Test
    fun shouldHaveEnabledConfirmButtonAfterSelectingPayByCard() {
        launchActivity()

        clickView(R.id.payByCardRadioButton)

        assertThatViewIsEnabled(R.id.confirmPurchaseButton)
    }

    @Test
    fun shouldSendOrderWhenConfirmButtonIsClicked() {
        given(checkoutService.sendOrder(any(), any())).willReturn(Completable.complete())
        given(checkoutService.checkoutPendingDrinkOrders()).willReturn(Completable.complete())
        launchActivity()

        clickView(R.id.payByCardRadioButton)
        clickView(R.id.confirmPurchaseButton)

        verify(checkoutService).sendOrder(any(), any())
    }

    @Test
    fun shouldNavigateToDrinksOrderOnCancelButtonClicked() {
        launchActivity()

        clickView(R.id.cancelButton)

        verify(navigationRouter).navigateToDrinksOrder()
    }

    private fun launchActivity() {
        activityTestRule.launchActivity(null)
    }
}