package co.instil.baristabobshop.ui

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import co.instil.baristabobshop.login.activity.LoginActivity
import co.instil.baristabobshop.R
import co.instil.baristabobshop.espresso.assertThatViewIsDisplayed
import co.instil.baristabobshop.espresso.assertThatViewIsEnabled
import co.instil.baristabobshop.espresso.assertThatViewIsNotEnabled
import com.nhaarman.mockito_kotlin.given
import io.reactivex.Completable
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import co.instil.baristabobshop.dagger.DaggerApplicationInitializer
import co.instil.baristabobshop.espresso.assertThatDialogWithTextIsDisplayed
import co.instil.baristabobshop.espresso.scrollToThenClickView
import co.instil.baristabobshop.espresso.scrollToThenTypeTextIntoView
import co.instil.baristabobshop.espresso.scrollToViewWithId
import co.instil.baristabobshop.espresso.typeTextIntoView
import co.instil.baristabobshop.login.service.LoginService
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import retrofit2.HttpException
import retrofit2.Response
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class LoginActivityTest {

    @Rule @JvmField val activityTestRule = ActivityTestRule<LoginActivity>(LoginActivity::class.java, true, true)
    @Rule @JvmField val daggerApplicationRule = DaggerApplicationInitializer(this)

    @Inject lateinit var loginService: LoginService
    @Inject lateinit var resourceService: ResourceService

    @Test
    fun shouldDisplayCoffeeIcon() {
        assertThatViewIsDisplayed(R.id.coffeeIconImageView)
    }

    @Test
    fun shouldDisplayUsernameEditText() {
        assertThatViewIsDisplayed(R.id.usernameEditText)
    }

    @Test
    fun shouldDisplayPasswordEditTest() {
        assertThatViewIsDisplayed(R.id.passwordEditText)
    }

    @Test
    fun shouldDisplayLoginButton() {
        assertThatViewIsDisplayed(R.id.loginButton)
    }

    @Test
    fun shouldDisableLoginAtStart() {
        assertThatViewIsNotEnabled(R.id.loginButton)
    }

    @Test
    fun shouldDisableLoginWhenNoUsernameEntered() {
        typeTextIntoView(R.id.passwordEditText, "AAA")

        scrollToViewWithId(R.id.loginButton)

        assertThatViewIsNotEnabled(R.id.loginButton)
    }

    @Test
    fun shouldCallLoginServiceLoginWhenLoginButtonClicked() {
        given(loginService.login(any(), any())).willReturn(Completable.complete())

        typeTextIntoView(R.id.usernameEditText, "AAA")
        scrollToThenTypeTextIntoView(R.id.passwordEditText, "AAA")
        scrollToThenClickView(R.id.loginButton)

        verify(loginService).login(any(), any())
    }

    @Test
    fun shouldDisableLoginWhenNoPasswordEntered() {
        typeTextIntoView(R.id.usernameEditText, "AAA")
        scrollToViewWithId(R.id.loginButton)

        assertThatViewIsNotEnabled(R.id.loginButton)
    }

    @Test
    fun shouldEnableLoginWhenBothUserNameAndPasswordEntered() {
        typeTextIntoView(R.id.usernameEditText, "AAA")
        scrollToThenTypeTextIntoView(R.id.passwordEditText, "AAA")
        scrollToViewWithId(R.id.loginButton)

        assertThatViewIsEnabled(R.id.loginButton)
    }

    @Test
    fun shouldShowErrorDialogWhenInvalidLogin() {
        given(loginService.login(any(), any())).willReturn(Completable.error(HttpException(Response.error<String>(505, mock()))))
        given(resourceService.getStringResource(R.string.login_error_dialog_title)).willReturn("ErrorTitle")
        given(resourceService.getStringResource(R.string.login_unknown_error_dialog_message)).willReturn("ErrorMessage")

        typeTextIntoView(R.id.usernameEditText, "AAA")
        scrollToThenTypeTextIntoView(R.id.passwordEditText, "AAA")
        scrollToThenClickView(R.id.loginButton)

        assertThatDialogWithTextIsDisplayed("ErrorMessage")
    }
}