package co.instil.baristabobshop.api

import co.instil.baristabobshop.Settings
import com.nhaarman.mockito_kotlin.mock
import okhttp3.mockwebserver.MockWebServer
import org.junit.rules.ExternalResource
import org.mockito.BDDMockito.given

class ApiServerMock : ExternalResource() {

    private val settings = mock<Settings>()

    val retrofitFactory = RetrofitFactory(settings)
    val server = MockWebServer()

    override fun before() {
        val port = server.port

        given(settings.apiUrl()).willReturn("http://localhost:$port")
    }

    override fun after() {
        server.shutdown()
    }
}