package co.instil.baristabobshop.scheduler

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ImmediateBaristaBobAndroidTestSchedulers : BaristaBobSchedulers {

    override fun uiScheduler(): Scheduler = AndroidSchedulers.mainThread()
    override fun ioScheduler(): Scheduler = Schedulers.trampoline()
}