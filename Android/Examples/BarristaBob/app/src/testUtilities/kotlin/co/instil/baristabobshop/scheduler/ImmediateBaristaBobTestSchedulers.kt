package co.instil.baristabobshop.scheduler

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class ImmediateBaristaBobTestSchedulers : BaristaBobSchedulers {

    override fun uiScheduler(): Scheduler = Schedulers.trampoline()
    override fun ioScheduler(): Scheduler = Schedulers.trampoline()
}