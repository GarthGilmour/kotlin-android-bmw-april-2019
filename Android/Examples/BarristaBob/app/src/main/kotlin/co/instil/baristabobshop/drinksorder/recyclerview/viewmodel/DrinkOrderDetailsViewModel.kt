/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder.recyclerview.viewmodel

import co.instil.baristabobshop.R
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.ui.service.ResourceService

class DrinkOrderDetailsViewModel(
    drinkOrder: DrinkOrderDetails,
    resourceService: ResourceService
) {

    private val costString = resourceService.getStringResource(R.string.format_two_decimal_place).format(drinkOrder.drink.cost)

    val name = drinkOrder.drink.name
    val cost = resourceService.getStringResource(R.string.gbp_symbol) + costString
    val quantity = resourceService.getStringResource(R.string.prefix_quantity) + drinkOrder.quantity
    val image = drinkOrder.drink.imageId
}