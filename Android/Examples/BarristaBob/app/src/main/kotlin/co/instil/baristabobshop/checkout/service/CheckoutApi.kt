/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.checkout.service

import io.reactivex.Completable
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

interface CheckoutApi {

    @POST("order")
    fun checkOut(
        @Header("Authorization") accessTokenHeader: String,
        @Body order: Order,
        @Query("paymentMethod") paymentMethod: String
    ): Completable
}
