package co.instil.baristabobshop.android.viewmodel

import android.support.v7.app.AppCompatActivity
import co.instil.baristabobshop.android.ui.dialog.DialogFactory
import co.instil.baristabobshop.scheduler.BaristaBobSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import javax.inject.Inject

abstract class ActivityViewModel {

    @Inject lateinit var resourceService: ResourceService
    @Inject lateinit var baristaBobSchedulers: BaristaBobSchedulers

    var dialogFactory: DialogFactory? = null

    open fun onCreate(activity: AppCompatActivity) {
        dialogFactory = DialogFactory(activity, resourceService)
    }

    open fun onDestroy() {
        dialogFactory = null
    }
}