package co.instil.baristabobshop.ui.view

import android.databinding.BindingAdapter
import android.widget.ImageView

@BindingAdapter("android:src")
fun setImageViewResource(imageView: ImageView, resource: Int) {
    imageView.setImageResource(resource)
}