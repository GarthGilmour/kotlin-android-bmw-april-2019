package co.instil.baristabobshop.security

import javax.crypto.Cipher
import javax.crypto.spec.GCMParameterSpec
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class EncryptionService @Inject constructor(
    private val keyStoreService: KeyStoreService,
    private val cipher: Cipher
) {

    fun encrypt(textToEncrypt: String): EncryptionResult {
        cipher.init(Cipher.ENCRYPT_MODE, keyStoreService.getOrGenerateSecretKey())
        return EncryptionResult(cipher.doFinal(textToEncrypt.toByteArray()), cipher.iv)
    }

    fun decrypt(cipherText: ByteArray, initializationVector: ByteArray): String {
        val gcmParameterSpec = GCMParameterSpec(128, initializationVector)
        cipher.init(Cipher.DECRYPT_MODE, keyStoreService.getOrGenerateSecretKey(), gcmParameterSpec)
        return String(cipher.doFinal(cipherText))
    }
}