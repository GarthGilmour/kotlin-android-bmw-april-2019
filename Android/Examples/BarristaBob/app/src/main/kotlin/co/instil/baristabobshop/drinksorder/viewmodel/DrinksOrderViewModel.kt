/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder.viewmodel
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import co.instil.baristabobshop.drinksorder.service.DrinksOrderService
import co.instil.baristabobshop.android.viewmodel.ActivityViewModel
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.navigation.NavigationRouter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.rx2.rxSingle
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DrinksOrderViewModel @Inject constructor(
    private val navigationRouter: NavigationRouter,
    private val drinksOrderService: DrinksOrderService
) : ActivityViewModel() {

    private var drinksOrderItemList = listOf<DrinkOrderDetails>()

    private val scope = CoroutineScope(Dispatchers.IO)

    val isOrderListEmpty = ObservableBoolean(true)
    val formattedCheckoutTotal = ObservableField<String>(getCheckoutTotalText())

    private fun getCheckoutTotalText(): String {
        return "Checkout £${totalString()}"
    }

    private fun totalString() = "%.2f".format(drinksOrderItemList.sumByDouble { it.quantity * it.drink.cost })

    fun onAddToOrderButtonClicked() {
        navigationRouter.navigateToDrinksMenu()
    }

    fun onCheckoutButtonClicked() {
        navigationRouter.navigateToCheckout()
    }

    fun getDrinkOrdersAndUpdateDetails() = scope.rxSingle {
        drinksOrderItemList = drinksOrderService.getAllDrinksOrderItems().await()
        isOrderListEmpty.set(drinksOrderItemList.isEmpty())
        formattedCheckoutTotal.set(getCheckoutTotalText())
        formattedCheckoutTotal.notifyChange()
        return@rxSingle drinksOrderItemList
    }
}