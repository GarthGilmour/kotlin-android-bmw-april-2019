package co.instil.baristabobshop.dagger

import co.instil.baristabobshop.Application
import co.instil.baristabobshop.android.ui.activity.BaseActivity
import co.instil.baristabobshop.checkout.activity.CheckoutActivity
import co.instil.baristabobshop.drinksmenu.activity.DrinksMenuActivity
import co.instil.baristabobshop.drinksorder.activity.DrinksOrderActivity
import co.instil.baristabobshop.drinksmenu.periodicmenuupdates.DrinksDatabaseUpdateWorker
import co.instil.baristabobshop.drinksmenu.recyclerview.adapter.DrinksMenuListAdapter
import co.instil.baristabobshop.login.activity.LoginActivity
import co.instil.baristabobshop.prompts.ToastPrompt
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface ApplicationComponent {

    fun inject(application: Application)

    fun inject(baseActivity: BaseActivity.Dependencies)

    fun inject(loginActivity: LoginActivity)

    fun inject(drinksMenuActivity: DrinksMenuActivity)

    fun inject(checkoutActivity: CheckoutActivity)

    fun inject(drinksOrderActivity: DrinksOrderActivity)

    fun inject(drinksDatabaseUpdateWorker: DrinksDatabaseUpdateWorker)

    fun inject(drinksMenuListAdapter: DrinksMenuListAdapter)

    fun inject(toastPrompt: ToastPrompt)
}