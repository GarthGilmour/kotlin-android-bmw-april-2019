package co.instil.baristabobshop.converters

import android.arch.persistence.room.TypeConverter
import java.util.Date

class Converters {

    @TypeConverter
    fun longToDate(value: Long?): Date? = value?.let { Date(it) }

    @TypeConverter
    fun dateToLong(date: Date?): Long? = date?.time
}