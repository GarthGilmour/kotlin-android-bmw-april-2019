/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.R
import co.instil.baristabobshop.android.ui.activity.BaseActivity
import co.instil.baristabobshop.databinding.ActivityDrinksOrderBinding
import co.instil.baristabobshop.drinksorder.recyclerview.adapter.DrinksOrderListAdapter
import co.instil.baristabobshop.drinksorder.viewmodel.DrinksOrderViewModel
import co.instil.baristabobshop.scheduler.BaristaBobSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_drinks_order.drinksOrderRecyclerView
import javax.inject.Inject

class DrinksOrderActivity : BaseActivity<DrinksOrderViewModel>() {

    @Inject lateinit var drinksOrderViewModel: DrinksOrderViewModel
    @Inject lateinit var resourceService: ResourceService
    @Inject lateinit var baristaBobSchedulers: BaristaBobSchedulers

    private val drinksOrderListAdapter by lazy {
        DrinksOrderListAdapter(resourceService)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Application.dependencies?.inject(this)
        attachViewModel(drinksOrderViewModel)

        DataBindingUtil.setContentView<ActivityDrinksOrderBinding>(this, R.layout.activity_drinks_order).apply {
            vm = drinksOrderViewModel
        }

        configureRecyclerView()
    }

    private fun configureRecyclerView() {
        drinksOrderRecyclerView.apply {
            adapter = drinksOrderListAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL))
        }
    }

    override fun onResume() {
        super.onResume()
        drinksOrderViewModel.getDrinkOrdersAndUpdateDetails()
            .subscribeOn(baristaBobSchedulers.ioScheduler())
            .observeOn(baristaBobSchedulers.uiScheduler())
            .subscribeBy(onSuccess = drinksOrderListAdapter::updateDrinkOrders)
    }
}