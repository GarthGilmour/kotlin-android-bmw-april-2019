package co.instil.baristabobshop

import android.support.multidex.MultiDexApplication
import co.instil.baristabobshop.dagger.AppModule
import co.instil.baristabobshop.dagger.ApplicationComponent
import co.instil.baristabobshop.dagger.DaggerApplicationComponent

open class Application : MultiDexApplication() {

    companion object {
        var dependencies: ApplicationComponent? = null
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    private fun initializeDagger() {
        dependencies = DaggerApplicationComponent
            .builder()
            .appModule(AppModule(this))
            .build()
        dependencies?.inject(this)
    }
}