package co.instil.baristabobshop.drinksmenu.service

import co.instil.baristabobshop.drink.persistence.Drink
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface DrinksMenuApi {

    @GET("menu")
    fun getDrinksMenuUpdate(
        @Header("Authorization")accessTokenHeader: String,
        @Query("timestamp") latestDrinkTimestamp: Long?
    ): Single<List<Drink>>
}