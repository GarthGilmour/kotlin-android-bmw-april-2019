/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.checkout.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.R
import co.instil.baristabobshop.android.ui.activity.BaseActivity
import co.instil.baristabobshop.checkout.recyclerview.adapter.CheckoutItemListAdapter
import co.instil.baristabobshop.checkout.viewmodel.CheckoutViewModel
import co.instil.baristabobshop.databinding.ActivityCheckoutBinding
import co.instil.baristabobshop.scheduler.BaristaBobSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_checkout.checkoutRecyclerView
import javax.inject.Inject

class CheckoutActivity : BaseActivity<CheckoutViewModel>() {

    @Inject lateinit var checkOutViewModel: CheckoutViewModel
    @Inject lateinit var resourceService: ResourceService
    @Inject lateinit var baristaBobSchedulers: BaristaBobSchedulers

    private val checkoutItemListAdapter by lazy {
        CheckoutItemListAdapter(resourceService)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Application.dependencies?.inject(this)
        attachViewModel(checkOutViewModel)

        val binding = DataBindingUtil.setContentView<ActivityCheckoutBinding>(this, R.layout.activity_checkout)
        binding.vm = checkOutViewModel

        configureRecyclerView()
    }

    private fun configureRecyclerView() {
        checkoutRecyclerView.apply {
            adapter = checkoutItemListAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(applicationContext, DividerItemDecoration.VERTICAL))
        }
    }

    override fun onResume() {
        super.onResume()

        checkOutViewModel.getDrinksOrdersAndUpdateDetails()
            .subscribeOn(baristaBobSchedulers.ioScheduler())
            .observeOn(baristaBobSchedulers.uiScheduler())
            .subscribeBy(onSuccess = checkoutItemListAdapter::updateDrinkOrders)
    }
}