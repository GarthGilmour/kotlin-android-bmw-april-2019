package co.instil.baristabobshop.dagger

import android.arch.persistence.room.Room
import android.content.Context
import androidx.work.WorkManager
import co.instil.baristabobshop.checkout.service.CheckoutApi
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.api.RetrofitFactory
import co.instil.baristabobshop.drink.persistence.DrinkDatabase
import co.instil.baristabobshop.drink.persistence.DrinkDatastore
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuApi
import co.instil.baristabobshop.login.service.LoginApi
import co.instil.baristabobshop.login.service.LoginService
import co.instil.baristabobshop.prompts.ToastPrompt
import co.instil.baristabobshop.scheduler.AndroidBaristaBobSchedulers
import co.instil.baristabobshop.scheduler.BaristaBobSchedulers
import co.instil.baristabobshop.security.JwtService
import co.instil.baristabobshop.services.Base64Service
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import java.security.KeyStore
import javax.crypto.Cipher
import javax.inject.Singleton

/**
 * Required to allow the application context to be available for Dagger to inject.
 */
@Module
class AppModule(val application: Application) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application

    @Provides
    @Singleton
    fun provideApplication(): Application = application

    @Provides
    @Singleton
    fun provideBaristaBobSchedulers(): BaristaBobSchedulers = AndroidBaristaBobSchedulers()

    @Provides
    @Singleton
    fun provideRetrofit(retrofitFactory: RetrofitFactory): Retrofit =
        retrofitFactory.createInstance()

    @Provides
    @Singleton
    fun provideLoginAPI(retrofitFactory: RetrofitFactory): LoginApi =
        retrofitFactory.createInstance()

    @Provides
    @Singleton
    fun provideDrinksMenuApi(retrofitFactory: RetrofitFactory): DrinksMenuApi =
        retrofitFactory.createInstance()

    @Provides
    @Singleton
    fun provideCheckoutApi(retrofitFactory: RetrofitFactory): CheckoutApi =
        retrofitFactory.createInstance()

    @Provides
    @Singleton
    fun providesKeyStore(): KeyStore {
        val keyStore = KeyStore.getInstance("AndroidKeyStore")
        keyStore.load(null)
        return keyStore
    }

    @Provides
    @Singleton
    fun providesCipher(): Cipher = Cipher.getInstance("AES/GCM/NoPadding")

    @Provides
    @Singleton
    fun provideLoginService(loginApi: LoginApi, base64Service: Base64Service, jwtService: JwtService): LoginService = LoginService(loginApi, base64Service, jwtService)

    @Provides
    @Singleton
    fun provideDrinkDatabase(context: Context): DrinkDatabase = Room.databaseBuilder(
        context,
        DrinkDatabase::class.java,
        "Drink_Database"
    ).fallbackToDestructiveMigration().build()

    @Provides
    fun provideDrinkDatastore(drinkDatabase: DrinkDatabase): DrinkDatastore =
        drinkDatabase.drinkDatastore()

    @Provides
    fun provideWorkManager(): WorkManager = WorkManager.getInstance()

    @Provides
    @Singleton
    fun provideToastPrompt(context: Context): ToastPrompt = ToastPrompt(context)
}