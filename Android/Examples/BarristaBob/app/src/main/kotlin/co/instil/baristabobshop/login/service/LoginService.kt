package co.instil.baristabobshop.login.service

import co.instil.baristabobshop.security.JwtService
import co.instil.baristabobshop.services.Base64Service
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.rx2.await
import kotlinx.coroutines.rx2.rxCompletable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class LoginService @Inject constructor(
    private val loginApi: LoginApi,
    private val base64Service: Base64Service,
    private val jwtService: JwtService
) {

    private val scope = CoroutineScope(Dispatchers.IO)

    lateinit var accessToken: String

    open fun login(username: String, password: String) = scope.rxCompletable {
        val usernamePassword = "$username:$password"
        val encodedUsernamePassword = base64Service.encodeToString(usernamePassword.toByteArray())
        val authorizationHeader = "Basic $encodedUsernamePassword"
        val response = loginApi.login(authorizationHeader).await()
        accessToken = response.token
        jwtService.storeJwt(accessToken)
    }
}