package co.instil.baristabobshop.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import co.instil.baristabobshop.checkout.activity.CheckoutActivity
import co.instil.baristabobshop.drinksmenu.activity.DrinksMenuActivity
import co.instil.baristabobshop.drinksorder.activity.DrinksOrderActivity
import co.instil.baristabobshop.login.activity.LoginActivity
import javax.inject.Inject
import kotlin.reflect.KClass

open class NavigationRouter @Inject constructor(
    private val context: Context
) {

    open fun navigateToLogin() = goToActivity(LoginActivity::class)

    open fun navigateToDrinksMenu() = goToActivity(DrinksMenuActivity::class)

    open fun navigateToDrinksOrder() = goToActivityAndClearTask(DrinksOrderActivity::class)

    open fun navigateToCheckout() = goToActivity(CheckoutActivity::class)

    private fun <T : Activity> goToActivity(activityClass: KClass<T>) {
        val activity = Intent(context, activityClass.java)
        activity.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(context, activity, null)
    }

    private fun <T : Activity> goToActivityAndClearTask(activityClass: KClass<T>) {
        val activity = Intent(context, activityClass.java)
        activity.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(context, activity, null)
    }
}