package co.instil.baristabobshop.services

import android.util.Base64
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Base64Service @Inject constructor() {

    fun encodeToString(byteArray: ByteArray) = Base64.encodeToString(byteArray, Base64.NO_WRAP)

    fun decodeToByteArray(string: String) = Base64.decode(string, Base64.NO_WRAP)
}