package co.instil.baristabobshop.scheduler

import io.reactivex.Scheduler

interface BaristaBobSchedulers {

    fun uiScheduler(): Scheduler

    fun ioScheduler(): Scheduler
}