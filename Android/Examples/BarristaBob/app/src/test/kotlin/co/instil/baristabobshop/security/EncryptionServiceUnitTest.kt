package co.instil.baristabobshop.security

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import java.security.Key
import javax.crypto.Cipher

class EncryptionServiceUnitTest {

    private val key = mock<Key>()
    private val keyStoreService = mock<KeyStoreService>()
    private val cipher = mock<Cipher>()

    private val decryptText = "text"
    private val cipherTextByteArray = ByteArray(0)
    private val initializationVector = ByteArray(12)

    private val target = EncryptionService(keyStoreService, cipher)

    @Test
    fun shouldEncryptAndReturnEncodedBytesAndInitializationVectorPair() {
        givenSuccessfullyEncryptsString()

        val result = target.encrypt(decryptText)

        assertThat(result, `is`(EncryptionResult(initializationVector, initializationVector)))
    }

    @Test
    fun shouldDecryptsCipherText() {
        givenSuccessfullyDecryptsCipherText()

        val result = target.decrypt(cipherTextByteArray, initializationVector)

        assertThat(result, `is`(decryptText))
    }

    private fun givenSuccessfullyEncryptsString() {
        given(keyStoreService.getOrGenerateSecretKey()).willReturn(key)
        given(cipher.iv).willReturn(initializationVector)
        given(cipher.doFinal(decryptText.toByteArray())).willReturn(initializationVector)
    }

    private fun givenSuccessfullyDecryptsCipherText() {
        val plainTextToByteArray = decryptText.toByteArray()
        given(cipher.doFinal(cipherTextByteArray)).willReturn(plainTextToByteArray)
    }
}