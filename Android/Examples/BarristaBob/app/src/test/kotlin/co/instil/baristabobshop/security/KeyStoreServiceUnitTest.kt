package co.instil.baristabobshop.security

import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test
import java.security.Key
import java.security.KeyStore

private const val secretKey = "secretKey"

class KeyStoreServiceUnitTest {

    private val key = mock<Key>()
    private val keyStore = mock<KeyStore>()
    private val keyGeneratorFactory = mock<KeyGeneratorFactory>()

    private val target = KeyStoreService(keyStore, keyGeneratorFactory)

    @Test
    fun shouldGetSecretKey() {
        given(keyStore.containsAlias(secretKey)).willReturn(true)
        given(keyStore.getKey(secretKey, null)).willReturn(key)

        target.getOrGenerateSecretKey()

        verify(keyStore).getKey(secretKey, null)
    }

    @Test
    fun shouldGenerateSecretKey() {
        given(keyStore.containsAlias(secretKey)).willReturn(false)

        target.getOrGenerateSecretKey()

        verify(keyGeneratorFactory).generateAndStoreNewKey()
    }
}