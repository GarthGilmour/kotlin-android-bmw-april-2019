/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksmenu.viewmodel

import co.instil.baristabobshop.drinksmenu.activity.DrinksMenuActivity
import co.instil.baristabobshop.drinksmenu.periodicmenuupdates.DrinksMenuUpdater
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuService
import co.instil.baristabobshop.scheduler.ImmediateBaristaBobTestSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Completable
import org.junit.Before
import org.junit.Test

class DrinksMenuViewModelUnitTest {

    private val resourceService = mock<ResourceService>()
    private val drinksMenuService = mock<DrinksMenuService>()
    private val drinksMenuActivity = mock<DrinksMenuActivity>()
    private val drinksMenuUpdater = mock<DrinksMenuUpdater>()

    private val target = DrinksMenuViewModel(drinksMenuService, drinksMenuUpdater)

    @Before
    fun beforeEachTest() {
        target.resourceService = resourceService
        target.baristaBobSchedulers = ImmediateBaristaBobTestSchedulers()
        target.dialogFactory = mock()
    }

    @Test
    fun shouldUpdateDrinksWhenViewModelCreated() {
        given(drinksMenuService.updateDrinksMenu()).willReturn(Completable.complete())

        target.onCreate(drinksMenuActivity)

        verify(drinksMenuService).updateDrinksMenu()
    }

    @Test
    fun shouldCallDrinksMenuServiceWhenGetAllDrinksCalled() {
        target.getAllDrinks()

        verify(drinksMenuService).getAllDrinks()
    }
}