/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksmenu.periodicmenuupdates

import androidx.work.WorkManager
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test

class DrinksMenuUpdaterUnitTest {

    private val workManager = mock<WorkManager>()

    private val target = DrinksMenuUpdater(workManager)

    @Test
    fun shouldCallEnqueuePeriodicWorkWhenScheduleIsCalled() {
        target.schedulePeriodicDrinksMenuUpdate()

        verify(workManager).enqueueUniquePeriodicWork(any(), any(), any())
    }
}