/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.extensions

import org.springframework.util.Base64Utils
import org.springframework.web.reactive.function.server.ServerRequest

fun ServerRequest.basicAuthHeader(): String {
    val authHeader = authHeader().replace("Basic", "").trim()
    return String(Base64Utils.decodeFromString(authHeader))
}

fun ServerRequest.jwtAuthHeader() =
        authHeader().replace("Bearer", "").trim()

fun ServerRequest.authHeader() =
        headers().header("Authorization").firstOrNull().orEmpty()