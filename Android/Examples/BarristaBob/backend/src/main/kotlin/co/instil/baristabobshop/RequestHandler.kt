/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop

import co.instil.baristabobshop.extensions.basicAuthHeader
import co.instil.baristabobshop.security.JwtAuthenticator
import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.ServerResponse.badRequest
import org.springframework.web.reactive.function.server.ServerResponse.noContent
import org.springframework.web.reactive.function.server.ServerResponse.ok
import org.springframework.web.reactive.function.server.ServerResponse.status
import reactor.core.publisher.Mono
import java.time.OffsetDateTime
import java.util.Date
import java.util.UUID

class RequestHandler(private val jwtAuthenticator: JwtAuthenticator) {

    fun login(request: ServerRequest): Mono<ServerResponse> {
        val usernamePassword = request.basicAuthHeader().split(":")
        val username = usernamePassword[0]
        val password = usernamePassword[1]

        if (username.contains("@") && password == "password") {
            val token = jwtAuthenticator.generateToken(username)
            return ok().body(BodyInserters.fromObject(LoginResponse(token)))
        }
        return status(HttpStatus.UNAUTHORIZED).build()
    }

    fun menu(request: ServerRequest): Mono<ServerResponse> {
        val timestamp = request.queryParam("timestamp").orElse("0").toLong()

        if (timestamp <= 0) {
            return ok().body(BodyInserters.fromObject(menuItems.subList(0, menuItems.size - 1)))
        }
        return ok().body(BodyInserters.fromObject(listOf(menuItems.last())))
    }

    fun placeOrder(request: ServerRequest) =
            request.bodyToMono(Order::class.java).flatMap(::handleOrder)

    private fun handleOrder(order: Order) =
            if (order.items.size > 5) badRequest().build() else ServerResponse.ok().build()

    private val menuItems: List<MenuItem> by lazy {
        listOf(
                MenuItem(name = "Americano", description = "Espresso with some added hot water", cost = "1.50"),
                MenuItem(name = "Latte", description = "Steamed milk with a single shot!", cost = "2.00"),
                MenuItem(name = "Espresso", description = "The original, for the coffee experts! ", cost = "1.30"),
                MenuItem(name = "Cappucino", description = "Most popular Coffee!", cost = "2.20"),
                MenuItem(name = "Tea", description = "English Classic", cost = "1.20"),
                MenuItem(name = "Water", description = "Straight from the tap", cost = "0.75"),
                MenuItem(name = "Special Water", description = "I'm running out of drinks", cost = "1.35"),
                MenuItem(name = "Protein Shake", description = "Why come to a cafe for one?", cost = "4.00"),
                MenuItem(name = "Hot Chocolate", description = "Creamy and Choccy", cost = "2.50")
        )
    }

}

data class LoginResponse(val token: String)

data class MenuItem(
    val name: String,
    val description: String,
    val cost: String,
    val imageId: Int = 2131165269,
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    val timeStamp: Date = Date()
)

data class Order(val items: List<Long>)