/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop

import co.instil.baristabobshop.extensions.jwtAuthHeader
import co.instil.baristabobshop.security.JwtAuthenticator
import org.springframework.core.io.ClassPathResource
import org.springframework.http.HttpStatus.UNAUTHORIZED
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter
import org.springframework.web.reactive.function.server.RouterFunctions
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse.status
import org.springframework.web.reactive.function.server.router
import reactor.ipc.netty.http.server.HttpServer

fun main(args: Array<String>) {
    val requestHandler = RequestHandler(jwtAuthenticator)
    val routes = router {
        "/api".nest {
            POST("/login", requestHandler::login)
            GET("/menu", requestHandler::menu)
            POST("/order", requestHandler::placeOrder)
        }

        resources("/**", ClassPathResource("static/"))
    }

    val authenticatedRoutes = routes.filter { request, next ->
        if (userAuthorized(request)) next.handle(request) else status(UNAUTHORIZED).build()
    }

    val httpHandler = RouterFunctions.toHttpHandler(authenticatedRoutes)
    val adapter = ReactorHttpHandlerAdapter(httpHandler)
    val server = HttpServer.create("localhost", 8080)
    server.startAndAwait(adapter)
}

private fun userAuthorized(request: ServerRequest) =
        request.path().contains("/login") || jwtAuthenticator.isTokenValid(request.jwtAuthHeader())

private val jwtAuthenticator: JwtAuthenticator by lazy { JwtAuthenticator() }