package com.joocy.fragments

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.pokemon_list_content.view.*

class PokemonListAdapter(
    private val monsters: List<PokemonRepository.Pokemon>,
    private val selectionHandler: PokemonSelectionHandler
) :
    RecyclerView.Adapter<PokemonListAdapter.ViewHolder>() {

    private val onClickListener = View.OnClickListener { view ->
        val item = view.tag as PokemonRepository.Pokemon
        selectionHandler.onSelection(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pokemon_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pokemon = monsters[position]
        holder.nameView.text = pokemon.name
        holder.primaryTypeView.setColorFilter(ContextCompat.getColor(holder.itemView.context, pokemon.primaryType.color))
        if (pokemon.secondaryType != null) {
            holder.secondaryTypeView.visibility = ImageView.VISIBLE
            holder.secondaryTypeView.setColorFilter(ContextCompat.getColor(holder.itemView.context, pokemon.secondaryType.color))
        }

        with(holder.itemView) {
            tag = pokemon
            setOnClickListener(onClickListener)
        }
    }

    override fun getItemCount() = monsters.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nameView: TextView = view.name
        val primaryTypeView: ImageView = view.primaryType
        val secondaryTypeView: ImageView = view.secondaryType
    }
}