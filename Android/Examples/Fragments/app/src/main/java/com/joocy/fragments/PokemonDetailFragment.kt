package com.joocy.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_pokemon_detail.*
import kotlinx.android.synthetic.main.pokemon_detail.view.*

class PokemonDetailFragment : Fragment() {

    companion object {
        const val ARG_MONSTER_NAME = "monster_name"

        fun display(monsterName: String, placeholderViewId: Int, fragmentManager: FragmentManager) {
            val fragment = PokemonDetailFragment().apply {
                arguments = Bundle().apply {
                    putString(PokemonDetailFragment.ARG_MONSTER_NAME, monsterName)
                }
            }
            fragmentManager.beginTransaction().replace(placeholderViewId, fragment).commit()
        }

    }

    private var monster: PokemonRepository.Pokemon? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(ARG_MONSTER_NAME)) {
                val monsterName = it.getString(ARG_MONSTER_NAME)
                if (monsterName != null) {
                    monster = PokemonRepository.monsterMap[monsterName]
                }
                activity?.toolbar_layout?.title = monster?.name
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.pokemon_detail, container, false)

        monster?.let {
            rootView.types.text = formatTypes(it)
            rootView.hp.text = it.hp.toString()
            rootView.attack.text = it.attack.toString()
            rootView.defense.text = it.defense.toString()
            if (it.evolvesInto != null) {
                rootView.evolutionParent.visibility = View.VISIBLE
                rootView.evolves.text = it.evolvesInto.name
            }
        }

        return rootView
    }

    private fun formatTypes(monster: PokemonRepository.Pokemon): String {
        val primaryType = monster.primaryType.displayName
        if (monster.secondaryType != null) {
            return "$primaryType, ${monster.secondaryType.displayName}"
        }
        return primaryType
    }
}
