package com.joocy.services

import android.content.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val TAG = "MainActivity"
    }

    lateinit var messageService: MessageService
    var messageServiceConnection = object: ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as MessageService.MessageBinder
            messageService = binder.messageService
            Log.i(TAG, "Bound to message service")
        }

        override fun onServiceDisconnected(name: ComponentName) {
            Log.i(TAG, "Unbound from message service")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClick(view: View) {
        forecastButton.isEnabled = false
        forecastText.text = messageService.getMessage()
        val location = forecastLocationText.text.toString()
        val day = forecastDayText.text.toString()
        WeatherService.startForecast(applicationContext, location, day)
    }

    val forecastReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.i(TAG, "Handling broadcast")
            val forecast = intent?.getParcelableExtra<WeatherForecast>(EXTRA_FORECAST)
            Log.i(TAG, "Forecast received: $forecast")
            forecastText.text = forecast?.forecast
            forecastButton.isEnabled = true
        }
    }

    override fun onStart() {
        super.onStart()
        Log.i(TAG, "Registering broadcast receiver")
        LocalBroadcastManager.getInstance(applicationContext)
            .registerReceiver(forecastReceiver, IntentFilter(FORECAST))
        Intent(this, MessageService::class.java).also {intent ->
            bindService(intent, messageServiceConnection, Context.BIND_AUTO_CREATE)
        }
    }

    override fun onStop() {
        super.onStop()
        Log.i(TAG, "Unregistering broadcast receiver")
        LocalBroadcastManager.getInstance(applicationContext)
            .unregisterReceiver(forecastReceiver)
        unbindService(messageServiceConnection)
    }

}
