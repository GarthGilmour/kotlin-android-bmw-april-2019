package com.joocy.debts

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.joocy.debts.data.Debt
import java.math.BigDecimal
import java.text.NumberFormat
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

class DebtListAdapter internal constructor(context: Context): RecyclerView.Adapter<DebtListAdapter.DebtViewHolder>() {

    private val inflater = LayoutInflater.from(context)
    private var debts = emptyList<Debt>()

    inner class DebtViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val creditorItemView = itemView.findViewById<TextView>(R.id.creditor)
        val amountItemView = itemView.findViewById<TextView>(R.id.amount)
        val dateItemView = itemView.findViewById<TextView>(R.id.date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DebtViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_debt_item, parent, false)
        return DebtViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: DebtViewHolder, position: Int) {
        val current = debts[position]
        holder.creditorItemView.text = current.creditor
        val amountAndReason = "${formatAmount(current.amount)} for ${current.reason}"
        holder.amountItemView.text = amountAndReason
        holder.dateItemView.text = formatDate(current.date)
    }

    override fun getItemCount() = debts.size

    fun setDebts(debts: List<Debt>) {
        this.debts = debts
        notifyDataSetChanged()
    }

    private fun formatAmount(amount: BigDecimal): String {
        return NumberFormat.getCurrencyInstance().format(amount)
    }

    private fun formatDate(date: ZonedDateTime): String {
        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        return formatter.format(date)
    }
}