package com.joocy.debts

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_ioulist.*
import kotlinx.android.synthetic.main.content_ioulist.*

class DebtListActivity : AppCompatActivity() {

    private lateinit var debtViewModel: DebtViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_ioulist)

        val adapter = DebtListAdapter(this)
        val debtListView = debtList
        debtListView.adapter = adapter
        debtListView.layoutManager = LinearLayoutManager(this)

        debtViewModel = ViewModelProviders.of(this).get(DebtViewModel::class.java)
        debtViewModel.debts.observe(this, Observer {
                debts -> debts?.let {adapter.setDebts(it)}
        })

        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }

}

