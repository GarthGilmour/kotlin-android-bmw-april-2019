package com.joocy.tests

import com.google.common.truth.Truth.assertThat
import org.junit.Assert
import org.junit.Test
import java.lang.Exception
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class NameFormatterTest {

    @Test
    fun should_format_name() {
        val name = "test"
        val formatter = NameFormatter("%s")
        val formattedName = formatter.format(name)
        assertThat(formattedName).isEqualTo(name)
    }

    @Test
    fun should_format_multiple_names() {
        val firstname = "Joe"
        var lastname = "Bloggs"
        val formatter = NameFormatter("%s, %s")
        val formattedName = formatter.format(lastname, firstname)
        assertThat(formattedName).isEqualTo("Bloggs, Joe")
    }

    @Test
    fun should_not_throw_when_too_few_arguments() {
        val formatter = NameFormatter("%s %s")
        try {
            val formattedName = formatter.format("Joe")
        } catch (e: Exception) {
            Assert.fail("Should not throw exception")
        }
    }

    @Test
    fun should_keep_placeholders_with_missing_values_in_result() {
        val formatter = NameFormatter("%s %s %s")
        val formattedName = formatter.format("Joe")
        assertThat(formattedName).isEqualTo("Joe %s %s")
    }
}
