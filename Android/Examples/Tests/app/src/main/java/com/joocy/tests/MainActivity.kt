package com.joocy.tests

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val nameFormatter = NameFormatter("Well, hello there %s!")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClick(view: View) {
        val intent = Intent(this, NameActivity::class.java)
        startActivityForResult(intent, NameActivity.REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == NameActivity.REQUEST_CODE) {
            when(resultCode) {
                Activity.RESULT_CANCELED -> renderNoName()
                Activity.RESULT_OK -> renderName(data)
            }
        }
    }

    private fun renderNoName() {
        nameText.text = getString(R.string.no_name)
    }

    private fun renderName(data: Intent?) {
        if (data != null) {
            val name = data.getStringExtra(NameActivity.EXTRA_NAME)
            nameText.text = nameFormatter.format(name)
        } else {
            renderNoName()
        }
    }

}
