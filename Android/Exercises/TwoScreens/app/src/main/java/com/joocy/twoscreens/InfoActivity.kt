package com.joocy.twoscreens

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {

    companion object {
        val INFO_REQUEST_CODE = 1
        val EXTRA_NAME = "name"
    }
   override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        pikachu.setOnClickListener {
            val intent = Intent().apply {
                putExtra(EXTRA_NAME, "Pikachu")
            }
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

    }
}
