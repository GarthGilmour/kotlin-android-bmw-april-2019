package com.joocy.myfirstapp

import org.junit.Test
import com.google.common.truth.Truth.assertThat

class ExampleUnitTest {
    @Test
    fun shouldAddTwoNumbersCorrectly() {
        val actualValue = 2 + 2
        assertThat(actualValue).isEqualTo(4)
    }
}

